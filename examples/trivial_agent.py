#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import sys
from time import sleep
from optparse import OptionParser

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    from sleekxmpp.util.misc_ops import setdefaultencoding

    setdefaultencoding('utf8')
else:
    raw_input = input

from siotox.thing_agent import ThingAgent
from siotox.sensor.generic.trivial import Trivial as Trivial_Sensor
from siotox.sensor.tplink.hs1x0 import HS1X0 as HS110_Sensor
from siotox.control.generic.trivial import Trivial as Trivial_Control


if __name__ == '__main__':
    #-------------------------------------------------------------------------------------------
    #   Parsing Arguments
    #-------------------------------------------------------------------------------------------

    optp = OptionParser()

    # Output verbosity options.
    optp.add_option('-q', '--quiet', help='set logging to ERROR',
                    action='store_const', dest='loglevel',
                    const=logging.ERROR, default=logging.INFO)
    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")

    opts, args = optp.parse_args()

    # Setup logging.
    logging.basicConfig(level=opts.loglevel,
                        format='%(levelname)-8s %(message)s')

    if opts.jid is None or opts.password is None :
        optp.print_help()
        exit()

    #-------------------------------------------------------------------------------------------
    #   Starting Agent
    #-------------------------------------------------------------------------------------------

    # Instance Agent
    TrivialAgent = ThingAgent(opts.jid , opts.password)

    # Add Elements
    TrivialAgent.add_sensor_element( "trivial", Trivial_Sensor() )
    #TrivialAgent.addSensorElement( "hs1x0_relay", HS110_Sensor( "192.168.1.9" ) )
    TrivialAgent.add_control_element( "trivial", Trivial_Control() )
    #while not (xmpp.testForRelease()):
    TrivialAgent.connect( use_tls = False )
    TrivialAgent.process(block=False)
    TrivialAgent.publish()
    while True:
        sleep(300)
        TrivialAgent.publish()
        
    logging.debug("lost connection")