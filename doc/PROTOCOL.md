
Feature list

Agent side

urn:xmpp:siotox:0:thing
urn:xmpp:siotox:0:thing:sensor
urn:xmpp:siotox:0:thing:sensor:pubsub
urn:xmpp:siotox:0:thing:control

Client Side

urn:xmpp:siotox:0:thing:manager

Ad-hoc commands

siotox:getdata
siotox:setdata

Default fields

siotox:datetime

PEP node for pubsub

urn:xmpp:siotox:0:thing:sensor:pubsub