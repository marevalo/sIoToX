Already done

- Thing Agent: Basic get data
- Thing Agent: Basic set data
- Thing Agent: Basic PubSub
- Thing Agent: Basic authorization: own roster open

To do before 1.0

- Thing Manager: Client connection wrapper
- Thing: API for the client side
- Advanced get and set (async)
- Advanced PubSub (auto publish)
- Thing Agent: Basic getinfo
- Full python CLI client
- Pythonize all the things
- Fix thread-trickiness
- Refator elements & devices
- Add more basic usecases ( raspberry, chip, etc.)


To do after 1.0

- Add chat interface ( getinfo, getdata, setdata, subscribe )
- Android graphic client
- Python graphic client