#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simplet IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévao
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
import sys
import pprint
from datetime import datetime

import sleekxmpp
from sleekxmpp.exceptions import XMPPError
from sleekxmpp.roster.multi import Roster

# Python versions before 3.0 do not use UTF-8 encoding
# by default. To ensure that Unicode is handled properly
# throughout SleekXMPP, we will set the default encoding
# ourselves to UTF-8.
if sys.version_info < (3, 0):
    from sleekxmpp.util.misc_ops import setdefaultencoding

    setdefaultencoding('utf8')
else:
    raw_input = input


class ThingAgent(sleekxmpp.ClientXMPP):
    """ An agent for a sIoToX Thing.

    This class will handle all Ad-Hoc commands and SD semantics needed for
    sIoToX. You will be able to register sensor and control Elements to
    it.
    """
    
    # Authorization is roster by default, the most XMPP one
    author_mode = "roster"
    ': :type author_mode: str'
    
    # If the thing is a sensor and a list of control elements
    is_sensor = False
    sensor_elements = {}
    ': :type sensor_elements: list(siotox.sensor.element.Element)'
    
    # If the thing is a control and a list of control elements
    is_control = False
    control_elements = {}
    ': :type author_mode: list(siotox.control.element.Element)'
    
    
    def __init__(self, jid, password):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self._start)
        self.add_event_handler("session_end", self._end)
        #self.add_event_handler("message", self.message)
        self.releaseMe = False
        
        # Register plugins for the basic blocks of the protocol
        # Service Discovery -> For publishing features
        self.register_plugin('xep_0030')
        # Data Forms -> Data syntax
        self.register_plugin('xep_0004')
        # Adhoc Commands -> remote call
        self.register_plugin('xep_0050')
        
        # Register plugins for the advanced blocks of the protocol
        self.register_plugin('xep_0060')
        self.register_plugin('xep_0115')
        self.register_plugin('xep_0163')

    def _start(self, event):
        logging.debug( '-Started the agent' )

        # We add the command after session_start has fired
        # to ensure that the correct full JID is used.

        # Publish I'm a thing, but still don't know if either a
        # sersor or control
        
        self['xep_0030'].add_feature('urn:xmpp:siotox:0:thing' , None , self.boundjid)
        
        if self.is_sensor :
            self['xep_0030'].add_feature('urn:xmpp:siotox:0:thing:sensor')
            self['xep_0050'].add_command(node='siotox:getdata',
                                         name='Get Data',
                                         handler=self._handle_get_command)
        if self.is_control :
            self['xep_0030'].add_feature('urn:xmpp:siotox:0:thing:control')
            self['xep_0050'].add_command(node='siotox:setdata',
                                         name='Set Data',
                                         handler=self._handle_set_command)
        #if self.is_pubsub :
        self['xep_0030'].add_feature(
                        'urn:xmpp:siotox:0:thing:sensor:pubsub')

        # Send presence & get roster
        self.send_presence( )
        self.get_roster()
        
        # By default do not auto authorize nor subscribe
        self.auto_authorize = False
        self.auto_subscribe = False
        
        # When starting I should start all elements
        logging.debug( '-Starting elements' )
        for key , element in self.sensor_elements.items() :
            logging.debug( '--Starting sensor:' + key  )
            element.start()
        for key , element in self.control_elements.items() :
            logging.debug( '--Starting control:' + key  )
            element.start()
        
    def _end(self, event):
        logging.debug( '-Ending the agent' )
        # When ending I should end all elements
        logging.debug( '-Ending elements' )
        for key , element in self.sensor_elements.items() :
            logging.debug( '--Ending sensor:' + key  )
            element.end()
        for key , element in self.control_elements.items() :
            logging.debug( '--Ending control:' + key  )
            element.end()

    def _get_data_form(self) :
        form = self['xep_0004'].makeForm('form', 'siotox:sensor_data')
        form.setType( 'result' )
        form.addField( 
            var = "siotox:datetime" ,
            ftype = "string",
            value = datetime.now().isoformat('T') )
        for key , element in self.sensor_elements.items() :
            form.addField(
                var= key ,
                ftype= element.get_f_type(),
                label= element.get_label() ,
                value= element.get_value() )
        return form
    
    def _set_data_form(self) :
        form = self['xep_0004'].makeForm('form', 'siotox:sensor_data')
        form.setType( 'form' )
        for key , element in self.control_elements.items() :
            form.addField(
                var= key ,
                ftype= element.get_f_type(),
                label= element.get_label() ,
                value= element.get_value() )
        return form
    
    def _handle_get_command(self, iq, session):
        logging.debug( '--Got a getdata command' )

        # Apply access rights
        # TODO: Learn how to properly return errors
        if not self._authorized( iq ):
            session['notes']= { 'error' : 'Authorization error' }
            return session
        
        # So if I can I will send my data
        session['payload'] = self._get_data_form()
        session['next'] = None
        session['has_next'] = False

        return session

    def _handle_set_command(self, iq, session):
        logging.debug( '--Got a setdata command, sending command form' )

        # Apply access rights
        # TODO: Learn how to properly return errors
        if not self._authorized( iq ):
            session['notes']= { 'error' : 'Authorization error' }
            session['has_next'] = False
            return session
        
        # So if I can I will send my data
        session['payload'] = self._set_data_form()
        session['next'] = self._handle_command_set_complete
        session['has_next'] = True
        session['allow_complete'] = False

        return session

    def _handle_command_set_complete(self, payload, session):
        logging.debug( '--Got a setdata command, receiving submit form' )
        
        # In this case (as is typical), the payload is a form
        form = payload

        logging.debug( "Received form: "+str(form) )
            
        fields = form.get_fields()
        for var in fields:
            self.control_elements[var].set_value( fields[var]['value'] )

        session['payload'] = None
        session['next'] = None
        session['allow_complete'] = False

        return session

    def _authorized(self , iq ):
        """
        Validate if the sender of the iq is authorized to read/write
        
        :type iq: sleekxmpp.stanza.Iq
        """
        to_bare_id = sleekxmpp.jid.JID(iq['to']).bare;
        from_bare_id = sleekxmpp.jid.JID(iq['from']).bare;
        # In open mode everybody can read/write
        if self.author_mode == "open":
            return True
        # In own mode only the own jid of the agent can read/write
        if self.author_mode == "own" and to_bare_id == from_bare_id:
            return True
        # In roster mode only the jid subscribed to me can read/write
        if self.author_mode == "roster" and from_bare_id in self._from_roster():
            return True
        return False
    
    def _from_roster(self):
        """
        Get the complete list of jids with at least from subscriptions
        """
        roster_from_list = []
        for key in self.client_roster:
            node = self.client_roster[key]
            if node['subscription'] == "both" or node['subscription'] == "from":
                roster_from_list.append( key )
        return roster_from_list
    
    def testForRelease(self):
        # todo thread safe
        return self.releaseMe

    def doReleaseMe(self):
        # todo thread safe
        self.releaseMe = True
        
    def set_author_mode( self , mode ):
        """ Set the security model for reading and writing data

        Arguments:
            mode -- One of these three modes:
                    own -- Only the same JID can read & write
                    roster -- Anybody subscribed to me can read & write
                    open -- Anybody can read & write
        """
        if mode not in ( "own", "roster", "open" ):
            raise ValueError( 'Invalid author mode. Valid modes: own, roster, open' )
        
        self.author_mode = mode

    def add_sensor_element(self , element_id , sensor_element  ):
        if not self.is_sensor:
            # Now I know I'm at least a sensor
            logging.debug( '--Now I\'m a sensor' )
            self.is_sensor = True
            
        # TODO: Should I check I'm a real element?
        self.sensor_elements[element_id] = sensor_element

    def add_control_element(self , element_id , control_element  ):
        if not self.is_control:
            # Now I know I'm at least a control
            logging.debug( '--Now I\'m a control' )
            self.is_control = True

        # TODO: Should I check I'm a real element?
        self.control_elements[element_id] = control_element
        
    def publish(self):
        # On demand publish of data
        self['xep_0163'].publish(
            self._get_data_form() ,
            "urn:xmpp:siotox:1:thing:sensor:pubsub" )
        logging.debug( '########################################################################################################################################' )
        