#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
import sys
import Adafruit_DHT
from siotox.sensor.element import Element

class DHT(Element):
    """
    Basic implementation for getting the temperature from
    DHT11, DHT22 or AM2302 with Adafruit library
    """

    def __init__( self , sensor , pin ):
        logging.debug( "DHT sensor element object started" )
        # Initializing members
        self.name = "temp"
        self.label = "Temperature"
        self.f_type = "string"
        self.sensor = sensor
        self.pin = pin

    def refresh(self):
        """
        override method to do the refresh work
        refresh values from hardware or other
        """
        pass


    # Get data from the plug
    def get_value(self):
        logging.debug( 'DHT received read data' )
        humidity, temperature = Adafruit_DHT.read_retry( self.sensor, self.pin)
        return str(temperature)
    
    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type