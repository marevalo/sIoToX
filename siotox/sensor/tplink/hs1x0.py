#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    Copyright (C) 2016 softScheck GmbH ( tplink-smartplug code )
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
import socket
from siotox.sensor.element import Element

class HS1X0(Element):
    """
    Basic implementation for getting the relay status on
    TP Link HS1X0
    """

    def __init__( self , ip ):
        logging.debug( "Trivial sensor element object started" )
        # Initializing members
        self.name = "hs1x0_relay"
        self.label = "HS1X0 Relay Status"
        self.f_type = "boolean"
        try:
            socket.inet_pton(socket.AF_INET, ip)
        except socket.error:
            quit("Invalid IP Address.") 
        self.hs_ip = ip
        logging.debug( 'Got the IP:' + ip )


    def refresh(self):
        """
        override method to do the refresh work
        refresh values from hardware or other
        """
        pass


    def _encrypt( self , string):
        key = 171
        result = "\0\0\0\0"
        for i in string: 
            a = key ^ ord(i)
            key = a
            result += chr(a)
            return result

    def _decrypt( self , string):
        key = 171 
        result = ""
        for i in string: 
            a = key ^ ord(i)
            key = ord(i) 
            result += chr(a)
        return result

    # Get data from the plug
    def get_value(self):
        logging.debug( 'hs1x0 received read data' )
        try:
            sock_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock_tcp.connect((self.hs_ip, 9999))
            command = '{"system":{"get_sysinfo":{}}}'
            logging.debug( 'Sending command: ' + command )
            sock_tcp.sendall( self._encrypt( command ))
            logging.debug( 'Command sent' )
            data = sock_tcp.recv(2048)
            sock_tcp.close()
            logging.debug( 'Result received: ' + data )     
            result = self._decrypt(data[4:])
            logging.debug( 'Result received: ' + result )            
        except socket.error:
            quit("Could not connect to host " + self.hs_ip + ":" + str(9999))
        return True
    
    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type