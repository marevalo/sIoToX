#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

# Generic imports
import datetime
import logging

# sIoToX imports
from siotox.control.element import Element

# Hardware imports
import RPi.GPIO as GPIO

class IO(Element):
    """
    Raspberry Pi's GPIO as a control
    """

    def __init__( self , channel ):
        GPIO.setmode(GPIO.BCM)
        logging.debug( "Raspberry Pi GPIO control element object started" )
        # Initializing members
        self.name = "GPIO_" + str(channel)
        self.label = "GPIO " + str(channel) + " control"
        self.f_type = "boolean"
        self.channel = channel
        self.initial = False
    
    def set_initial(self , value ):
        """
        Set initial value, to be executed between __init__() and start()
        """
        self.initial = value
        
    def start(self):
        GPIO.setup(self.channel, GPIO.OUT , initial=self.initial )
        
    def end(self):
        GPIO.cleanup(self.channel)
        
    def get_value(self):
        return ( GPIO.input( self.channel ) == 1 )
    
    def set_value(self , value ):
        GPIO.output( self.channel, value )

    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type