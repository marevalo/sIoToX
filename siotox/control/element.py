#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

import datetime
import logging

class Element(object):
    """
    Example implementation of a control element
    """

    def __init__( self , name , label , f_type ):
        logging.debug( "Sensor element object started" )
        # Initializing members
        self.name = name
        self.label = label
        self.f_type = f_type

    def start(self):
        """
        override method to do the start work
        this start will be done after setting up XMPP session
        on thing_agent->_start()
        """
        pass
    
    def end(self):
        """
        override method to do the end work
        this ending will be done when ending the XMPP session
        on thing_agent->_end()
        """
        pass
    
    def get_value(self):
        return None
    
    def set_value(self , value ):
        pass

    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type