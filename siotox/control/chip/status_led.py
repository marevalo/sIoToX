#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
from siotox.control.generic.command_switch import CommandSwitch

class StatusLed(CommandSwitch):
    """
    Status LED of a NTC C.H.I.P. board
    
    Should be run with root account
    """

    def __init__( self ):
        logging.debug( "CHIP's status LED element object started" )
        # Initializing members
        self.name = "chip_status_led"
        self.label = "CHIP's status LED"
        self.f_type = "boolean"

        self.set_prepare_command( "echo none > /sys/devices/platform/leds/leds/chip\:white\:status/trigger" )
        self.set_switch_on_command( "i2cset -f -y 0 0x34 0x93 0x1" )
        self.set_switch_off_command( "i2cset -f -y 0 0x34 0x93 0x0" )
        self.set_release_command( "echo heartbeat > /sys/devices/platform/leds/leds/chip\:white\:status/trigger" )