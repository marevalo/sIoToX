#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
from os import system
from siotox.control.element import Element

class CommandSwitch(Element):
    """
    Example implementation of a switch controlled by command line
    """

    prepare_command = ""
    switch_on_command = ""
    switch_off_command = ""
    release_command = ""
    status = False
    

    def __init__( self ):
        logging.debug( "Command switch element object started" )
        # Initializing members
        self.name = "command_switch"
        self.label = "Command Switch"
        self.f_type = "boolean"
        
    def end(self):
        self._command_line( self.release_command )

    def start(self):
        self._command_line( self.prepare_command )        

    def get_value(self):
        return self.status
    
    def set_prepare_command(self , command ):
        self.prepare_command = command 
    
    def set_release_command(self , command ):
        self.release_command = command 
    
    def set_switch_on_command(self , command ):
        self.switch_on_command = command 
    
    def set_switch_off_command(self , command ):
        self.switch_off_command = command 
    
    def set_value(self , value ):
        self.status = value
        if ( self.status ):
            self._command_line( self.switch_on_command )
        else:
            self._command_line( self.switch_off_command )
        pass

    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type
    
    def _command_line (self , command ):
        system( command )
        