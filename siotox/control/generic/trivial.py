#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    sIoToX : simple IoT over XMPP
    My own little framework to build a thermostat
    Copyright (C) 2017 Miguel A. Arévalo
    See the file LICENSE on this folder and up for copying permission.
"""

import logging
from siotox.control.element import Element

class Trivial(Element):
    """
    Example implementation of a control element
    """

    value = False

    def __init__( self ):
        logging.debug( "Trivial sensor element object started" )
        # Initializing members
        self.name = "trivial"
        self.label = "Trivial Actuator"
        self.f_type = "boolean"

    def get_value(self):
        return self.value 
    
    def set_value(self , value ):
        self.value = value
        pass

    def get_name(self):
        return self.name
    
    def get_label(self):
        return self.label
    
    def get_f_type(self):
        return self.f_type